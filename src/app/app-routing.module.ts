import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactosComponent } from './components/routes/contactos/contactos.component';
import { GaleriaComponent } from './components/routes/galeria/galeria.component';
import { InicioComponent } from './components/routes/inicio/inicio.component';
import { ServiciosComponent } from './components/routes/servicios/servicios.component';

const routes: Routes = [
  {
    path:'inicio', /*Esto es lo que va a mostrar en la URL*/
    component: InicioComponent
  },
  {
    path:'contactos',
    component: ContactosComponent
  },
  {
    path:'galeria',
    component: GaleriaComponent
  },
  
  {
    path:'servicios',
    component: ServiciosComponent
  },
  {
    path:'**', /*si en el link se escribe algo que no existe lo redirecciona al inicio*/
    redirectTo: 'inicio'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
